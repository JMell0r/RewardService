This is a Sample SpringBoot microservice based on Reward Service.
Usually written using DropWizard thought it would be good to try springboot for a change.

java version 8
springBootVersion = "2.0.0.RELEASE"
gradle wrapper 4.5.1
Lombok 1.16.20
jdbi 3

Flyway is installed to manage the database scripts

It can use the h2 database

When building please select the select gradle wrapper task configuration

the following completed tests are included no database is required or external services
required to run these

C:\repos-icc-3\RewardService\src\test\java\com\jmellor\rewards\controller\RewardControllerTest.java
C:\repos-icc-3\RewardService\src\test\java\com\jmellor\rewards\model\enums\ChannelTest.java
C:\repos-icc-3\RewardService\src\test\java\com\jmellor\rewards\model\enums\ChannelRewardTest.java
C:\repos-icc-3\RewardService\src\test\java\com\jmellor\rewards\model\PortfolioTest.java
C:\repos-icc-3\RewardService\src\test\java\com\jmellor\rewards\model\RewardsTest.java
C:\repos-icc-3\RewardService\src\test\java\com\jmellor\rewards\model\RewardTest.java
C:\repos-icc-3\RewardService\src\test\java\com\jmellor\rewards\model\enums\EligibilityTest.java
C:\repos-icc-3\RewardService\src\test\java\com\jmellor\rewards\service\EligibilityServiceTest.java
C:\repos-icc-3\RewardService\src\test\java\com\jmellor\rewards\service\RewardServiceTest.java

Cukes is installed to run too.
The Reward version test is coded and the Rewards.feature is in progress.
These tests will run via intellij but the cukes runner has been ignored as not all running yet.



 