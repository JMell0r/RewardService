package com.jmellor.rewards.model;


import com.jmellor.rewards.model.enums.Channel;
import com.jmellor.rewards.model.enums.ChannelReward;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Reward {

    private Channel channel;
    private ChannelReward channelReward;

    public Reward(String channel, String channelReward) {
        this.channel = Channel.valueOf(channel);
        this.channelReward = ChannelReward.valueOf(channelReward);
    }

}
