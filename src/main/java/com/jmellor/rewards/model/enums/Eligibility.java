package com.jmellor.rewards.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Eligibility {

    CUSTOMER_ELIGIBLE("CUSTOMER_ELIGIBLE", "Customer is eligible"),
    CUSTOMER_INELIGIBLE("CUSTOMER_INELIGIBLE", "Customer is not eligible"),
    TECHNICAL_FAILURE("TECHNICAL_FAILURE", "Service technical failure"),
    INVALID_ACCOUNT("INVALID_ACCOUNT", "The supplied account is invalid");

    private String name;
    private String description;
}
