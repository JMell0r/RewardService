package com.jmellor.rewards.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ChannelReward {

    CHAMPIONS_LEAGUE_FINAL_TICKET("CHAMPIONS_LEAGUE_FINAL_TICKET"),
    KARAOKE_PRO_MICROPHONE("KARAOKE_PRO_MICROPHONE"),
    PIRATES_OF_THE_CARABBEAN_COLLECTION("PIRATES_OF_THE_CARABBEAN_COLLECTION");

    private String reward;


}
