package com.jmellor.rewards.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Channel {

    SPORTS("SPORTS"),
    KIDS("KIDS"),
    MUSIC("MUSIC"),
    NEWS("NEWS"),
    MOVIES("MOVIES");

    private String name;
}
