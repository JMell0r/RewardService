package com.jmellor.rewards.model;


import com.jmellor.rewards.model.enums.Channel;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;


public class Rewards {

    private List<Reward> rewards;

    public Rewards(List<Reward> allRewards) {
        this.rewards = allRewards;
    }

    public List<Reward> asList() {
        return rewards;
    }

    public List<String> getChannelRewards(List<Channel> channels){

        List<String> result = new ArrayList<>();
        if (channels == null || channels.isEmpty()  ) return result;

        List<Channel> validChannels = new ArrayList<>();

       channels.forEach(channel -> {
           if (existChannel(channel)){
               validChannels.add(channel);
           }
       });

        List<Reward> validRewards = new ArrayList<>();

       validChannels.forEach(validChannel -> {
           validRewards.add(getReward(validChannel));
       });

        return validRewards.stream().map(reward -> reward.getChannelReward().getReward()).collect(toList());
    }

    public boolean existChannel(Channel channel){
        return rewards.stream().anyMatch(reward -> reward.getChannel().equals(channel));
    }

    public Reward getReward(Channel channel){
        return rewards.stream().filter(r -> r.getChannel().equals(channel)).findAny().orElse(null);

    }

}
