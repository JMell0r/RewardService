package com.jmellor.rewards.model;

import com.jmellor.rewards.model.enums.Channel;
import lombok.Getter;

import java.util.List;

@Getter
public class Portfolio {

    private List<Channel> channels;

    public Portfolio(List<Channel> channels) {
        this.channels = channels;
    }

}
