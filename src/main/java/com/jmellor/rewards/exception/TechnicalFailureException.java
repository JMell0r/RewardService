package com.jmellor.rewards.exception;

public class TechnicalFailureException extends Exception {

    public TechnicalFailureException(String message) {
        super(message);
    }
}
