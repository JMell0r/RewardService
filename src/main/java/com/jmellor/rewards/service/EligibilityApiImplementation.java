package com.jmellor.rewards.service;

import com.jmellor.rewards.exception.InvalidAccountException;
import com.jmellor.rewards.exception.TechnicalFailureException;
import com.jmellor.rewards.model.enums.Eligibility;
import org.springframework.stereotype.Service;

@Service
public class EligibilityApiImplementation implements  EligibilityServiceInterface{

    @Override
    public Eligibility callApi(int accountNumber) throws InvalidAccountException, TechnicalFailureException {
        return null;
    }
}
