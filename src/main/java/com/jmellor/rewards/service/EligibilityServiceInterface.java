package com.jmellor.rewards.service;

import com.jmellor.rewards.exception.InvalidAccountException;
import com.jmellor.rewards.exception.TechnicalFailureException;
import com.jmellor.rewards.model.enums.Eligibility;

public interface EligibilityServiceInterface {

    public Eligibility callApi(int accountNumber) throws InvalidAccountException, TechnicalFailureException;

}
