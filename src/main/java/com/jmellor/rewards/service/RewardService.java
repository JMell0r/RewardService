package com.jmellor.rewards.service;

import com.jmellor.rewards.dao.RewardDao;
import com.jmellor.rewards.model.Portfolio;
import com.jmellor.rewards.model.Reward;
import com.jmellor.rewards.model.Rewards;
import org.jdbi.v3.core.Jdbi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RewardService {

    private RewardDao rewardDao;
    private Rewards rewards;

   @Autowired
    public void rewardDao(Jdbi dbi) {
       rewardDao = dbi.onDemand(RewardDao.class);
    }
    public RewardService(){
       
    }
    public RewardService(RewardDao rewardDao) {
        this.rewardDao = rewardDao;
    }

    public Reward getRewardByChannel(String channel){
       return rewardDao.getRewardByChannel(channel);
    }

    public Rewards getAllRewards() {
       return new Rewards(rewardDao.getAllRewards());
    }

    public List<String> processPortfolio(Portfolio portfolio) {

       List<String> result =  new ArrayList<>();

       if (portfolio.getChannels() == null || portfolio.getChannels().isEmpty() ){
           return result;
       }

       if (rewards == null) rewards = getAllRewards();

       result = rewards.getChannelRewards(portfolio.getChannels());

       return  result;
    }

}
