package com.jmellor.rewards.service;

import com.jmellor.rewards.exception.InvalidAccountException;
import com.jmellor.rewards.exception.TechnicalFailureException;
import com.jmellor.rewards.model.enums.Eligibility;
import org.springframework.stereotype.Service;

import static com.jmellor.rewards.model.enums.Eligibility.INVALID_ACCOUNT;
import static com.jmellor.rewards.model.enums.Eligibility.TECHNICAL_FAILURE;

@Service
public class EligibilityService  {

    private EligibilityServiceInterface eligibilityServiceInterface;

    public EligibilityService(EligibilityServiceInterface eligibilityServiceInterface) {
        this.eligibilityServiceInterface = eligibilityServiceInterface;
    }

    public Eligibility process(int accountNumber) {

        Eligibility result ;

        try {
            // will call the realAPI
            result = eligibilityServiceInterface.callApi(accountNumber);
        } catch (InvalidAccountException e) {
            return INVALID_ACCOUNT;
        } catch (TechnicalFailureException e) {
            return TECHNICAL_FAILURE;
        }
        return result;

    }

}
