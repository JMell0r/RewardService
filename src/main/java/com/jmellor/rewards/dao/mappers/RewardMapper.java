package com.jmellor.rewards.dao.mappers;

import com.jmellor.rewards.model.Reward;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RewardMapper implements RowMapper<Reward> {
    @Override
    public Reward map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new Reward(rs.getString("channel"),
                rs.getString("reward"));
    }
}
