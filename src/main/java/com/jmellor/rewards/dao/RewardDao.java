package com.jmellor.rewards.dao;

import com.jmellor.rewards.dao.mappers.RewardMapper;
import com.jmellor.rewards.model.Reward;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface RewardDao {

    String selectFields = "r.channel, r.reward";
    String tableName = "rewards r";


    @UseRowMapper(RewardMapper.class)
    @SqlQuery(
            "SELECT " +
                    selectFields +
                    " FROM "
                    + tableName)
    List<Reward> getAllRewards();

    @UseRowMapper(RewardMapper.class)
    @SqlQuery(
            "SELECT " +
                    selectFields +
                    " FROM "
                    + tableName +
                    "WHERE " +
                    "channel = :channel")
    Reward getRewardByChannel(@Bind("channel") String channel);


}
