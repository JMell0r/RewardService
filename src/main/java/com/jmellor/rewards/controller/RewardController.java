package com.jmellor.rewards.controller;

import com.jmellor.rewards.exception.InvalidAccountException;
import com.jmellor.rewards.model.enums.Eligibility;
import com.jmellor.rewards.model.Portfolio;
import com.jmellor.rewards.service.EligibilityService;
import com.jmellor.rewards.service.RewardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static com.jmellor.rewards.model.enums.Eligibility.CUSTOMER_ELIGIBLE;
import static com.jmellor.rewards.model.enums.Eligibility.INVALID_ACCOUNT;

@RestController
@RequestMapping("/api/v1/rewards")
public class RewardController {

    @Autowired
    private RewardService rewardService;

    @Autowired
    private EligibilityService eligibilityService;

    @RequestMapping("/")
    public String home(){
        return "Reward Home check";
    }

    @RequestMapping(value = "/account/{accountNumber}", method = RequestMethod.GET)
    public ResponseEntity<?> getRewardByAccountNumber(@PathVariable("accountNumber") int accountNumber, @RequestBody Portfolio portfolio) throws InvalidAccountException {

        List<String> rewards = new ArrayList<>();

        Eligibility eligibility = eligibilityService.process(accountNumber);

        if (eligibility.equals(CUSTOMER_ELIGIBLE)){
           rewards = rewardService.processPortfolio(portfolio);
        }
        else if (eligibility.equals(INVALID_ACCOUNT)){
            throw new InvalidAccountException(INVALID_ACCOUNT.getDescription());
        }

        return new ResponseEntity<>(rewards, HttpStatus.OK );
    }

    @RequestMapping(method={RequestMethod.GET},value={"/version"})
    public String getVersion() {

        return "1.0";
    }
}
