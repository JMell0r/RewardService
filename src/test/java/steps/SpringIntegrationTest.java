package steps;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;
@Slf4j

public class SpringIntegrationTest {
    
    protected RestTemplate restTemplate;

    public SpringIntegrationTest() {
        restTemplate = new RestTemplate();
    }

    protected String executeGet(String url)  {
          return restTemplate.getForEntity(url,String.class).getBody();
    }

    protected int executeGetStatus(String url)  {
        return restTemplate.getForEntity(url,String.class).getStatusCodeValue();
    }

    void clean(String url) {
        restTemplate.delete(url);
    }

    int put(String url, final String something) {
        return restTemplate.postForEntity(url, something, Void.class).getStatusCodeValue();
    }

}
