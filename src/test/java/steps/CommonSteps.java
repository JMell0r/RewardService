package steps;

import com.jmellor.rewards.App;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;

@ContextConfiguration
@SpringBootTest(classes = App.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CommonSteps extends SpringIntegrationTest {

    String url = "http://localhost:8090/api/v1/rewards/version";
    String urlRewards = "http://localhost:8090/api/v1/rewards/account";

    private List<String> rewards;
    private  ResponseEntity<?> response;

    @When("^the client calls /version$")
    public void the_client_issues_GET_version() throws Throwable{
        executeGet(url);
    }

    @Then("^the client receives status code of (\\d+)$")
    public void the_client_receives_status_code_of(int statusCode) throws Throwable {
       int currentStatusCode =  executeGetStatus(url);
        assertThat(currentStatusCode, is(statusCode));
    }

    @And("^the client receives server version (.+)$")
    public void the_client_receives_server_version_body(String version) throws Throwable {
        assertThat( executeGet(url), is(version));
    }

    @And("^I expect the Number of rewards as follows \"([^\"]*)\" \"([^\"]*)\"$")
    public void iExpectTheNumberOfRewardsAsFollows(String accountId, String rewards) throws Throwable {

        List<String> returnedRewards = new ArrayList<>();
        assertEquals(returnedRewards.size(), rewards);
    }

    @Given("^Returned Customer Rewards$")
    public void returnedCustomerRewards() throws Throwable {

        ResponseEntity<?> responseLocal = response;
        rewards = (List<String>) responseLocal.getBody();
    }

    @Given("^Perform a GET on \"([^\"]*)\"$")
    public void performAGETOn(String accountId) throws Throwable {

        throw new PendingException();
    }
}
