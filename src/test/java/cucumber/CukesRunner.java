package cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.Ignore;
import org.junit.runner.RunWith;

@Ignore
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features/"
        ,plugin = {"pretty", "html:target/cucumber"}
        ,glue= "steps"
)
public class CukesRunner {}

