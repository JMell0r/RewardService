package com.jmellor.rewards.controller;

import com.jmellor.rewards.exception.InvalidAccountException;
import com.jmellor.rewards.model.Portfolio;
import com.jmellor.rewards.service.EligibilityService;
import com.jmellor.rewards.service.RewardService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.jmellor.rewards.model.enums.Channel.MUSIC;
import static com.jmellor.rewards.model.enums.Eligibility.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RewardControllerTest {

    private int elegibleAccountNumber =  1111;
    private int notElegibleAccountNumber =  2222;
    private int serviceDownAccountNumber = 7777;
    private int invalidAccountNumber = 9999;
    private Portfolio musicPortfolio;
    private final static String MUSIC_REWARD = "KARAOKE_PRO_MICROPHONE";
    List<String> musicRewards = Arrays.asList(MUSIC_REWARD);
    List<String> noRewards = new ArrayList<>();

    @InjectMocks
    private RewardController target ;

    @Mock
    EligibilityService eligibilityService;

    @Mock
    RewardService rewardService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        musicPortfolio = new Portfolio(Arrays.asList(MUSIC));
        when(eligibilityService.process(elegibleAccountNumber)).thenReturn(CUSTOMER_ELIGIBLE);
        when(eligibilityService.process(notElegibleAccountNumber)).thenReturn(CUSTOMER_INELIGIBLE);
        when(eligibilityService.process(serviceDownAccountNumber)).thenReturn(TECHNICAL_FAILURE);
        when(eligibilityService.process(invalidAccountNumber)).thenReturn(INVALID_ACCOUNT);
        when(rewardService.processPortfolio(musicPortfolio)).thenReturn(musicRewards);
    }

    @Test
    public void test_RewardHome(){
        String result = target.home();
        assertEquals( result, "Reward Home check");
    }

    @Test
    public void test_CustomerIsEligible() throws InvalidAccountException {
        ResponseEntity<?> response = target.getRewardByAccountNumber(elegibleAccountNumber, musicPortfolio);
        verify(rewardService, times(1)).processPortfolio(musicPortfolio);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertEquals(response.getBody(), musicRewards);
    }

    @Test
    public void test_CustomerIsNotEligible() throws InvalidAccountException {
        ResponseEntity<?> response = target.getRewardByAccountNumber(notElegibleAccountNumber, musicPortfolio);
        verify(rewardService, times(0)).processPortfolio(musicPortfolio);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertEquals(response.getBody(), noRewards );
    }

    @Test
    public void test_ServiceTechnicalFailure() throws InvalidAccountException {
        ResponseEntity<?> response = target.getRewardByAccountNumber(serviceDownAccountNumber, musicPortfolio);
        verify(rewardService, times(0)).processPortfolio(musicPortfolio);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertEquals(response.getBody(), noRewards );
    }

    @Test(expected = InvalidAccountException.class)
    public void test_InvalidAccountNumber() throws InvalidAccountException {
        ResponseEntity<?> response = target.getRewardByAccountNumber(invalidAccountNumber, musicPortfolio);
    }

}