package com.jmellor.rewards.service;

import com.jmellor.rewards.exception.InvalidAccountException;
import com.jmellor.rewards.exception.TechnicalFailureException;
import com.jmellor.rewards.model.enums.Eligibility;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.jmellor.rewards.model.enums.Eligibility.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class EligibilityServiceTest {

    private int elegibleAccountNumber =  1111;
    private int notElegibleAccountNumber =  2222;
    private int serviceDownAccountNumber = 7777;
    private int invalidAccountNumber = 9999;

    @Mock
    EligibilityApiImplementation eligibilityApiImplementation;

    EligibilityService target ;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        target = new EligibilityService(eligibilityApiImplementation);
        when(eligibilityApiImplementation.callApi(elegibleAccountNumber)).thenReturn(CUSTOMER_ELIGIBLE);
        when(eligibilityApiImplementation.callApi(notElegibleAccountNumber)).thenReturn(CUSTOMER_INELIGIBLE);
        when(eligibilityApiImplementation.callApi(serviceDownAccountNumber)).thenThrow(new TechnicalFailureException(TECHNICAL_FAILURE.name()));
        when(eligibilityApiImplementation.callApi(invalidAccountNumber)).thenThrow(new InvalidAccountException(INVALID_ACCOUNT.name()));

    }

    @Test
    public void test_CustomerEligible(){
        Eligibility result = target.process(elegibleAccountNumber);
        assertEquals(result, CUSTOMER_ELIGIBLE);
    }

    @Test
    public void test_CustomerInEligible(){
        Eligibility result = target.process(notElegibleAccountNumber);
        assertEquals(result, CUSTOMER_INELIGIBLE);
    }

    @Test
    public void test_InvalidAccountNumber() {
        Eligibility result = target.process(invalidAccountNumber);
        assertEquals(result, INVALID_ACCOUNT);
    }

    @Test
    public void test_TechnicalFailureNumber() {
        Eligibility result = target.process(serviceDownAccountNumber);
        assertEquals(result, TECHNICAL_FAILURE);
    }

}