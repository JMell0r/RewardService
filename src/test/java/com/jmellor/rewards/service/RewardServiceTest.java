package com.jmellor.rewards.service;

import com.jmellor.rewards.dao.RewardDao;
import com.jmellor.rewards.model.Portfolio;
import com.jmellor.rewards.model.Reward;
import com.jmellor.rewards.model.enums.Channel;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.jmellor.rewards.model.enums.Channel.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class RewardServiceTest {

    private RewardService target;
    private List<Channel> channels =  Arrays.asList(MUSIC,MOVIES);
    private List<Channel> channelsWithNotAvailable =  Arrays.asList(MUSIC,NEWS);
    private List<Channel> emptyChannels =   new ArrayList<>();
    private Portfolio portfolio ;
    private Portfolio portfolioOneValid ;
    private Portfolio emptyPortfolio ;
    private Reward rewardMusic = new Reward("MUSIC","KARAOKE_PRO_MICROPHONE");
    private Reward rewardMovies = new Reward("MOVIES","PIRATES_OF_THE_CARABBEAN_COLLECTION");
    private Reward rewardSports = new Reward("SPORTS","CHAMPIONS_LEAGUE_FINAL_TICKET");
    private List<Reward> rewards = Arrays.asList(rewardMusic, rewardMovies, rewardSports) ;

    @Mock
    RewardDao rewardDao;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        target = new RewardService(rewardDao);
        portfolio = new Portfolio(channels);
        portfolioOneValid = new Portfolio(channelsWithNotAvailable);
        emptyPortfolio = new Portfolio(emptyChannels);

        when(rewardDao.getAllRewards()).thenReturn(rewards);
    }

    @Test
    public void test_EmptyprocessPortfolio() {
        List<String> result =  target.processPortfolio(emptyPortfolio);
        assertEquals(result.isEmpty(), true);
    }

    @Test
    public void test_ProcessPortfolioWithAllRewards() {
        List<String> result =  target.processPortfolio(portfolio);
        assertEquals(result.size(), 2);
    }

    @Test
    public void test_ProcessPortfolioWithOneValidReward() {
        List<String> result =  target.processPortfolio(portfolioOneValid);
        assertEquals(result.size(), 1);
    }
}