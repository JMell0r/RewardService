package com.jmellor.rewards.model.enums;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ChannelRewardTest {

    @Test
    public void test_ChannelRewards() {
            assertThat(ChannelReward.CHAMPIONS_LEAGUE_FINAL_TICKET.getReward(),is(notNullValue()));
            assertThat(ChannelReward.KARAOKE_PRO_MICROPHONE.getReward(),is(notNullValue()));
            assertThat(ChannelReward.PIRATES_OF_THE_CARABBEAN_COLLECTION.getReward(),is(notNullValue()));
    }
}