package com.jmellor.rewards.model.enums;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class EligibilityTest {

    @Test
    public void test_EligibiltyContracts() {
        assertThat(Eligibility.CUSTOMER_ELIGIBLE.name(),is(notNullValue()));
        assertThat(Eligibility.CUSTOMER_INELIGIBLE.name(),is(notNullValue()));
        assertThat(Eligibility.INVALID_ACCOUNT.name(),is(notNullValue()));
        assertThat(Eligibility.TECHNICAL_FAILURE.name(),is(notNullValue()));
        assertThat(Eligibility.INVALID_ACCOUNT.getDescription(),is("The supplied account is invalid"));
    }
}