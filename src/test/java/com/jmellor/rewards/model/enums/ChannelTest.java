package com.jmellor.rewards.model.enums;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ChannelTest {

    @Test
    public void test_ChannelsExist() {
        assertThat(Channel.KIDS.name(),is(notNullValue()));
        assertThat(Channel.MOVIES.name(),is(notNullValue()));
        assertThat(Channel.MUSIC.name(),is(notNullValue()));
        assertThat(Channel.NEWS.name(),is(notNullValue()));
        assertThat(Channel.SPORTS.name(),is(notNullValue()));
    }

}