package com.jmellor.rewards.model;

import com.jmellor.rewards.model.enums.Channel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.jmellor.rewards.model.enums.Channel.MOVIES;
import static com.jmellor.rewards.model.enums.Channel.MUSIC;
import static com.jmellor.rewards.model.enums.Channel.NEWS;
import static org.junit.Assert.assertEquals;

public class RewardsTest {

    private Rewards validRewards ;

    private List<Channel> emptyChannels = new ArrayList<>();
    private List<Channel> nullChannels =  null;
    private List<Channel> validChannels = Arrays.asList(MUSIC,MOVIES);
    private List<Channel> validChannelsOneWithNoReward = Arrays.asList(MUSIC,NEWS);
    private Reward rewardMusic = new Reward("MUSIC","KARAOKE_PRO_MICROPHONE");
    private Reward rewardMovies = new Reward("MOVIES","PIRATES_OF_THE_CARABBEAN_COLLECTION");
    private Reward rewardSports = new Reward("SPORTS","CHAMPIONS_LEAGUE_FINAL_TICKET");

    @Before
    public void setUp() throws Exception {
        validRewards = new Rewards(Arrays.asList(rewardMusic, rewardMovies, rewardSports)) ;
    }

    @Test
    public void test_NullChannelRewards() {

        List<String> result = validRewards.getChannelRewards(nullChannels);
        assertEquals(result, Collections.emptyList());

    }

    @Test
    public void test_EmptyChannelRewards() {

        List<String> result = validRewards.getChannelRewards(emptyChannels);
        assertEquals(result, Collections.emptyList());

    }

    @Test
    public void test_GetChannelRewardsWithExistingChannels() {

      List<String> result =  validRewards.getChannelRewards(validChannels);
      assertEquals(result.size(),2 );
      assertEquals(result.toString(), "[KARAOKE_PRO_MICROPHONE, PIRATES_OF_THE_CARABBEAN_COLLECTION]");

    }

    @Test
    public void test_GetChannelRewardsWithExistingChannelAndOneThatDoesNotHaveAReward() {

        List<String> result =  validRewards.getChannelRewards(validChannelsOneWithNoReward);
        assertEquals(result.size(), 1);
        assertEquals(result.toString(), "[KARAOKE_PRO_MICROPHONE]");

    }

}