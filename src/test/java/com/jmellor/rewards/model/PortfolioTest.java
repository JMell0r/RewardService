package com.jmellor.rewards.model;

import com.jmellor.rewards.model.enums.Channel;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.jmellor.rewards.model.enums.Channel.MOVIES;
import static com.jmellor.rewards.model.enums.Channel.MUSIC;
import static org.junit.Assert.assertEquals;

public class PortfolioTest {

    private Portfolio target;
    private List<Channel> channels =  Arrays.asList(MUSIC,MOVIES);

    @Test
    public void test_Portfolio_Contents() {

        target = new Portfolio(channels);
        assertEquals(target.getChannels().size(), 2);

    }
}