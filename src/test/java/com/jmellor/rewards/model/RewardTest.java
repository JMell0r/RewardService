package com.jmellor.rewards.model;

import com.jmellor.rewards.model.enums.Channel;
import com.jmellor.rewards.model.enums.ChannelReward;
import org.junit.Test;

import static org.junit.Assert.*;

public class RewardTest {

    private Reward reward;

    @Test
    public void test_CheckSportsChannelAndChannelReward() {
        reward = new Reward("SPORTS","CHAMPIONS_LEAGUE_FINAL_TICKET");
        assertEquals(reward.getChannel(),Channel.SPORTS);
        assertEquals(reward.getChannelReward(),ChannelReward.CHAMPIONS_LEAGUE_FINAL_TICKET);
    }

    @Test
    public void test_CheckMoviesChannelAndChannelReward() {
        reward = new Reward("MOVIES","PIRATES_OF_THE_CARABBEAN_COLLECTION");
        assertEquals(reward.getChannel(),Channel.MOVIES);
        assertEquals(reward.getChannelReward(),ChannelReward.PIRATES_OF_THE_CARABBEAN_COLLECTION);
    }

    @Test
    public void test_CheckMusicChannelAndChannelReward() {
        reward = new Reward("MUSIC","KARAOKE_PRO_MICROPHONE");
        assertEquals(reward.getChannel(),Channel.MUSIC);
        assertEquals(reward.getChannelReward(),ChannelReward.KARAOKE_PRO_MICROPHONE);
    }

}