$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("RewardVersion.feature");
formatter.feature({
  "line": 1,
  "name": "the Reward version can be retrieved",
  "description": "",
  "id": "the-reward-version-can-be-retrieved",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "client makes call to GET /version",
  "description": "",
  "id": "the-reward-version-can-be-retrieved;client-makes-call-to-get-/version",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "the client calls /version",
  "keyword": "When "
});
formatter.step({
  "line": 5,
  "name": "the client receives status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "the client receives server version 1.0",
  "keyword": "And "
});
formatter.match({
  "location": "CommonSteps.the_client_issues_GET_version()"
});
formatter.result({
  "duration": 1282811544,
  "error_message": "org.springframework.web.client.ResourceAccessException: I/O error on GET request for \"http://localhost:8090/rewards/version\": Connection refused: connect; nested exception is java.net.ConnectException: Connection refused: connect\r\n\tat org.springframework.web.client.RestTemplate.doExecute(RestTemplate.java:743)\r\n\tat org.springframework.web.client.RestTemplate.execute(RestTemplate.java:686)\r\n\tat org.springframework.web.client.RestTemplate.getForEntity(RestTemplate.java:361)\r\n\tat steps.SpringIntegrationTest.executeGet(SpringIntegrationTest.java:16)\r\n\tat steps.CommonSteps.the_client_issues_GET_version(CommonSteps.java:32)\r\n\tat ✽.When the client calls /version(RewardVersion.feature:4)\r\nCaused by: java.net.ConnectException: Connection refused: connect\r\n\tat java.net.DualStackPlainSocketImpl.connect0(Native Method)\r\n\tat java.net.DualStackPlainSocketImpl.socketConnect(DualStackPlainSocketImpl.java:79)\r\n\tat java.net.AbstractPlainSocketImpl.doConnect(AbstractPlainSocketImpl.java:350)\r\n\tat java.net.AbstractPlainSocketImpl.connectToAddress(AbstractPlainSocketImpl.java:206)\r\n\tat java.net.AbstractPlainSocketImpl.connect(AbstractPlainSocketImpl.java:188)\r\n\tat java.net.PlainSocketImpl.connect(PlainSocketImpl.java:172)\r\n\tat java.net.SocksSocketImpl.connect(SocksSocketImpl.java:392)\r\n\tat java.net.Socket.connect(Socket.java:589)\r\n\tat java.net.Socket.connect(Socket.java:538)\r\n\tat sun.net.NetworkClient.doConnect(NetworkClient.java:180)\r\n\tat sun.net.www.http.HttpClient.openServer(HttpClient.java:463)\r\n\tat sun.net.www.http.HttpClient.openServer(HttpClient.java:558)\r\n\tat sun.net.www.http.HttpClient.\u003cinit\u003e(HttpClient.java:242)\r\n\tat sun.net.www.http.HttpClient.New(HttpClient.java:339)\r\n\tat sun.net.www.http.HttpClient.New(HttpClient.java:357)\r\n\tat sun.net.www.protocol.http.HttpURLConnection.getNewHttpClient(HttpURLConnection.java:1220)\r\n\tat sun.net.www.protocol.http.HttpURLConnection.plainConnect0(HttpURLConnection.java:1156)\r\n\tat sun.net.www.protocol.http.HttpURLConnection.plainConnect(HttpURLConnection.java:1050)\r\n\tat sun.net.www.protocol.http.HttpURLConnection.connect(HttpURLConnection.java:984)\r\n\tat org.springframework.http.client.SimpleBufferingClientHttpRequest.executeInternal(SimpleBufferingClientHttpRequest.java:78)\r\n\tat org.springframework.http.client.AbstractBufferingClientHttpRequest.executeInternal(AbstractBufferingClientHttpRequest.java:48)\r\n\tat org.springframework.http.client.AbstractClientHttpRequest.execute(AbstractClientHttpRequest.java:53)\r\n\tat org.springframework.web.client.RestTemplate.doExecute(RestTemplate.java:729)\r\n\tat org.springframework.web.client.RestTemplate.execute(RestTemplate.java:686)\r\n\tat org.springframework.web.client.RestTemplate.getForEntity(RestTemplate.java:361)\r\n\tat steps.SpringIntegrationTest.executeGet(SpringIntegrationTest.java:16)\r\n\tat steps.CommonSteps.the_client_issues_GET_version(CommonSteps.java:32)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:37)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:13)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:31)\r\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\r\n\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\r\n\tat cucumber.runtime.Runtime.runStep(Runtime.java:299)\r\n\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\r\n\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:91)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:93)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:37)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:98)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:137)\r\n\tat com.intellij.junit4.JUnit4IdeaTestRunner.startRunnerWithArgs(JUnit4IdeaTestRunner.java:68)\r\n\tat com.intellij.rt.execution.junit.IdeaTestRunner$Repeater.startRunnerWithArgs(IdeaTestRunner.java:47)\r\n\tat com.intellij.rt.execution.junit.JUnitStarter.prepareStreamsAndStart(JUnitStarter.java:242)\r\n\tat com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:70)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 35
    }
  ],
  "location": "CommonSteps.the_client_receives_status_code_of(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "1.0",
      "offset": 35
    }
  ],
  "location": "CommonSteps.the_client_receives_server_version_body(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.uri("Rewards.feature");
formatter.feature({
  "line": 1,
  "name": "Rewards",
  "description": "",
  "id": "rewards",
  "keyword": "Feature"
});
});